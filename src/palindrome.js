export class palindrome {
    constructor () {
        this.reGex;
        this.lowReGex;
        this.reverseSentence;
    }

    reGex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g;

    checkPalindrome(sentence) {

        this.lowReGex = sentence.toLowerCase().replace(this.reGex,'');
        //comparo sentence y reGex
        //pongo en minúscula sentence
        //creo nueva cadena con las coincidencias entre reGex y sentence
        //y quito los espacios, puntos...
        
        this.reverseSentence = this.lowReGex.split('').reverse().join('');
        //separo las letras de lowRegex
        //le doy la vuelta al resultado del paso anterior (invierto el orden)
        //junto el resultado de las letras anteriores

        if (this.reverseSentence === this.lowReGex) {
            return "es palíndromo"
        } else {
            return "no es palíndromo"
        }

    }

}