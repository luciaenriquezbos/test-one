export class nifVerification {
    constructor () {
        this.number;
        this.lett;
        this.letter;
        this.regularExpression;
    }
    regularExpression = /^\d{8}[a-zA-Z]$/;

    verification(dni) {

        if (this.regularExpression.test(dni) == true) {
            this.number = dni.substr(0, dni.length - 1) // extraigo numero del dni. longitud del dni - 1
            this.lett = dni.substr(dni.length - 1, 1) // extraigo letra dni. string de 2 caracter que empieza en la posicion de la long total - 1
            this.letter = 'TRWAGMYFPDXBNJZSQVHLCKE';
            this.number = this.number % 23; // indice de la letra en el string, le añado 1 porque los string empiezan por 1
            this.letter = this.letter.substring(this.number, this.number + 1);

            if (this.letter != this.lett.toUpperCase()) {
                return "DNI incorrecto, error en la letra"
            } else {
                return "DNI correcto"
            }
        } else {
            return "DNI incorrecto, error de formato"
        }
    }
};