import { nifVerification } from "../src/dni";

it( "verification", () => {
    const dniC = "12345688r";
    const dniL = "77848272H";
    const dniW = "77";

    const nif = new nifVerification;

    expect (nif.verification(dniC)).toBe("DNI correcto")
    expect (nif.verification(dniL)).toBe("DNI incorrecto, error en la letra")
    expect (nif.verification(dniW)).toBe("DNI incorrecto, error de formato")

} )