import { palindrome } from "../src/palindrome";

it ("checkPalindrome", () => {
    const a = "Logra Casillas allí sacar gol";
    const b = "Esto no es";

    const check = new palindrome;

    expect(check.checkPalindrome(a)).toBe("es palíndromo");
    expect(check.checkPalindrome(b)).toBe("no es palíndromo");

})